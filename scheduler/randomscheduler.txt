MP2 (Partie I - Q3,Q4): Ordonnanceurs

Q3.Étude du code du simulateur.
Q4.Description du fonctionnement de la fonction randomscheduler dans un fichier randomscheduler.txt.
scheduler/sched.c/tproc * randomscheduler(tlist * procs, tlist * ready, int * delta)

Sachant que le simulateur est composé de plusieurs fichiers dont :
	|-sched.c: qui contient la logique du simulateur et ce dernier contient des fonctions dont :
		|- randomscheduler: c’est un ordonnanceur exemple 
		|- fcfs, rr, sjf, srtf: ces fonctions implémentent différents ordonnanceurs. Pour l’instant elles appellent toutes, randomscheduler.

Alors, procédons comme suit (les types et structures de données utilisées, le prototype puis le contenu de la fonction) :

	I) Types et structures de données utilisées :

	    Dans le fichier sched.h : 
		    On a une structure de données qui décrit ce qu'on va appeler un processus (typedef tproc) (ligne 22-28):
		    tproc est un type pointeur de fonctions pour les fonctions d’ordonnancement.
			/* process */
			typedef struct {
			    int pid;          // un pid son identidiant
			    int activation;   // un temps d’activation
			    int length;       // une durée d’exécution
			    int remaining;    // pour savoir à tout instant combien de temps il reste à exécuter
			} tproc;

		Donc sched.h: décrit les différents types utilisés.

		    tproc: définit un processus, il comporte un pid, un temps d’activation, une durée d’exécution (length) et un champ remaining pour savoir à tout instant combien de temps il reste à exécuter.
		    tnode: un nœud dans une liste chaînée de processus
		    tlist: une liste chaînée de processus
		    tstats: une structure pour comptabiliser différentes métriques (temps de complétion, attente et réponse).
		    tproc: est un type pointeur de fonctions pour les fonctions d’ordonnancement
		    un ensemble de fonctions pour manipuler les listes de processus sont également définies

		Donc sched.c :

		    tlist ready : List of ready procs Ou liste des processus prêts
		    tlist procs : List of other procs Ou liste des autres processus

	    Donc list.c: contient l’implémentation des fonction manipulant les listes de processus
		    add: permet d’ajouter un processus en fin de liste
		    del: permet de supprimer un processus
		    Pour l de type tlist*, l->first et l->last permettent d’accéder au premier et dernier éléments de la liste
		    Pour n de type tnode*, n->next permet d’accéder à l’élément suivant dans la liste et n->proc retourne la structure processus.

	II) Description du prototype: 	tproc * randomscheduler(tlist * procs, tlist * ready, int * delta)

	    Entrées :
	   		@param		tlist * procs		de type tlist *, c'est un pointeur sur la liste chaînée des autres processus
	   		@param		tlist * ready		de type tlist *, c'est un pointeur sur la liste chaînée des processus prêts
	   		@param		int * delta			de type int *, c'est un pointeur sur un entier qui contient le nombre d’unités de temps pendant
	   														 lesquelles le processus sera exécuté.

	   	Sorties :
	   		@return     tproc *				tproc * est un type pointeur de fonction, la focntion renvoie un pointeur sur le processus
	   										 aux fonctions d’ordonnancement (fcfs,rr,sjf,srtf).

	III) Description du contenu (corps de la fonction):

			/* --Scheduler random--*/
			tproc * randomscheduler(tlist * procs, tlist * ready, int * delta)
			{
		1-    int length = len(ready);
		2-    int selected = rand()%length;
		3-    tnode * p = ready->first;
		4-    for (int i=0; i < selected; i++) 
			  {
		5-        p = p->next;
              }
		6-    *delta = rand()%p->proc->remaining + 1;
		7-    return p->proc;
			}

		1 - Création et récupération de la longueur de la liste chaîné des processus prêt avec la fonction len(list l) dans l'entier length.
		La fonction len(list l) est définit comme suit dans le header (sched.h) et c'est le nombre de processus de la liste.

		2- Création et récupération de la valeur numérique de (rand()%length) dans l'entier selected.
		La valeur de ce rapport est un entier aléatoire qui sera le nombre de processus prêt à sélectionner.

		1-2: Donc la fonction sélectionne aléatoirement un nombre de processus parmis la liste des processus prêts.

		3 - Création d'un noeud p (tnode * p), qui pointe sur le premier élément de la liste des processus prêts
		Sachant le nombre de processus prêt sélectionné, donc la fonction accède au premier processus de la liste des processus prêts.

		4,5- Pour le nombre de processus prêt(s) sélectionné(s), il stocke pour chacun dans leur p->next, l'adresse du processus suivant.

		6- Réccupère dans la variable delta du processus sélectionné, une valeur aléatoire qui est le nombre d’unités de temps restant
		pendant lesquelles le processus sera exécuté.

		7- Renvoie la structure du processus p .

		En résumé :
		----------
		Elle ordonnance aléatoirement les processus présents dans la liste des processus prêts et renvoie la structure processus p à exécuter.

		Pour ce faire, avec la liste des processus prêts :
		- Elle sélectionne aléatoirement un nombre de processus parmi la liste des processus prêts.
		- Sachant le nombre de processus prêt sélectionné, donc la fonction accède au premier processus de la liste des processus prêts
		dont elle stocke l'adresse dans p.
		- Pour le nombre de processus prêt(s) sélectionné(s), il stocke pour chacun dans leur p->next, l'adresse du processus suivant
		ce qui permet d’accéder à l’élément suivant de la liste s'il y en a.
		- Puis elle définit dans un entier delta, le nombre d’unités de temps restant pendant lesquelles le processus p sera exécuté.
		- Et renvoie la structure du processus p .