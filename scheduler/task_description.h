/*
// donnees initales
int max_time = 20;
tproc tasks[] = {
    {1,0,3,3},
    {2,2,6,6},
    {3,4,4,4},
    {4,6,5,5},
    {5,8,2,2}
};
//*/

/*Q16 
int max_time = 40;
tproc tasks[] = {
     //pid  //activation  //length  //remaining  //period
    {1,     0,            1,        1,           3       },
    {2,     0,            1,        1,           4       },
    {3,     0,            2,        2,           6       },
};
//*/

///*
//Q22 - ordonnancable avec EDF et qui n'est pas ordonnancable avec RM. 
int max_time = 40;
tproc tasks[] = {
     //pid  //activation  //length  //remaining  //period
    {1,     0,            1,        1,           5       },
    {2,     0,            4,        4,           6       },
    {3,     0,            1,        1,           9       },

};
//*/