/* Copyright (c) 2013 Pablo Oliveira <pablo.oliveira@prism.uvsq.fr>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.  All rights reserved.
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "sched.h"

/* --Scheduler random--*/
tproc * randomscheduler(tlist * procs, tlist * ready, int * delta) {
    int length = len(ready);
    int selected = rand()%length;
    tnode * p = ready->first;
    for (int i=0; i < selected; i++) {
        p = p->next;
    }
    *delta = rand()%p->proc->remaining + 1;
    return p->proc;
}
/* --Scheduler random--*/


/* --Scheduler fcfs-- */
tproc * fcfs(tlist * procs, tlist * ready, int * delta) {
/* 
    Principe :
        - le scheduler fcfs accede au premier processus
          de la liste des processus prets.
        - ecrit dans delta, le nombre d'unites de temps
          restant pendant lesquelles process sera execute.
        - renvoie la structure du processus process.

    Entrees :
        @param  tlist * procs   c'est un pointeur sur la liste
                                 des autres processus.
        @param  tlist * ready   c'est un pointeur sur la liste
                                des processus prets.
        @param  int * delta     c'est un pointeur sur un entier
                                qui contient le nombre d'unites
                                de temps pendant lesquelles 
                                le processus process sera execute.

    Sorties :
        @return     tproc *     tproc * est un type pointeur de fonction,
                                la fonction renvoie la structure du processus. 
*/

    tnode * process = ready->first;
    * delta = process->proc->remaining;

    return process->proc;  
}
/* --Scheduler fcfs-- */


/* --Scheduler rr-- */
tproc * rr(tlist * procs, tlist * ready, int * delta) {
/*
    Principe :
        - On accede au premier processus de la liste
         des processus prets.
        - Alors le premier processus de la liste est
          remplace par son successeur, car le premier.
          processus est supprimer en tete de liste des
         processus prets et ajoute en fin de liste des
          autres processus.
        - Donc ajout du processus en fin de la liste
         des autres processus afin que nous puissions
         avoir l'ordonnancement en tourniquet.
        - comme en cours, on prend le quantum q est fixe a 1. 
        - Avec n processus, Chaque execution dure au
         maximum quantum unites de temps donc : delta = q = 1.

    Entrees :
        @param  tlist * procs   c'est un pointeur sur 
                                la liste des autres processus.
        @param  tlist * ready   c'est un pointeur sur 
                                la liste des processus prets.
        @param  int * delta     c'est un pointeur sur un entier
                                qui contient le nombre d'unites de temps 
                                pendant lesquelles le processus
                                process sera execute.

    Sorties :
        @return     tproc *     la fonction renvoie la structure du processus 
*/

    tnode * process = ready->first;
    ready->first = ready->first->next;

    del(ready, process->proc);                 
    add(procs, process->proc);
    
    * delta = QUANTUM;

    return process->proc;
}
/* --Scheduler rr-- */

/* --Scheduler sjf-- */
tproc * sjf(tlist * procs, tlist * ready, int * delta) {
/*
    Principe :
        - On suppose que les processus dans l'ordre 1, 2, . . . , k.
        - On accede au premier processus de la liste des processus prets
        - vu qu'ils sont dans l'ordre on suppose le premier processus
          a la plus courte duree d'execution
        - Soient les durees d'execution p1, p2, ... , pk 
        - On recherche le job le plus court de la liste 
         (celui ayant le plus petit pk) en parcourant 
          la liste des processus prets 
            -  si on trouve un processus ayant une duree plus courte 
                que celui actuellement connu, alors: 
                -  ce dernier processus avec le min(pk) 
                   devient ce lui ayant le plus petit job.
            - et ainsi de suite jusqu'a la fin du parcours
        - On renvoie le processus ayant la plus courte duree pour execution. 

    Entrees :
        @param  tlist * procs   c'est un pointeur sur la liste 
                                des autres processus.
        @param  tlist * ready   c'est un pointeur sur la liste
                                des processus prets.
        @param  int * delta     c'est un pointeur sur un entier
                                qui contient le nombre d'unites
                                de temps restant pendant lesquelles
                                le processus process sera execute.

    Sorties :
        @return     tproc *     Renvoie la structure du processus 
                                ayant le plus petit job
*/

    tnode * process = ready->first;
    tnode * tmp = ready->first;
    tproc * plusCourtProcess = process->proc;

    for (; tmp != NULL; tmp = tmp->next)
        if((tmp->proc->length) < (plusCourtProcess->length))
            plusCourtProcess = tmp->proc;       

    *delta = plusCourtProcess->length;

    return plusCourtProcess;
}
/* --Scheduler sjf-- */

/* --Scheduler srtf-- */
tproc * srtf(tlist * procs, tlist * ready, int * delta) {
/*
    Principe :
        - On suppose que l'on execute les processus dans 
          l'ordre 1, 2, . . . , k
        - On accede au premier processus de la liste des
           processus prets
        - On suppose le premier est le plus court
        - Soient les durees d'execution p1, p2, ... , pk 
        - On recherche la plus courte duree en parcourant
          la liste des processus prets 
            -  si on trouve un processus ayant une duree 
               d'execution plus petite que le temps restant
               au processus courant: 
                -  ce dernier devient donc le plus court
            - et ainsi de suite jusqu'a la fin du parcours
        - On fixe delta = q = 1
        - On renvoie le plus court processus obtenu

    Entrees :
        @param  tlist * procs   c'est un pointeur sur la 
                                liste des autres processus
        @param  tlist * ready   c'est un pointeur sur la
                                liste des processus prets
        @param  int * delta     c'est un pointeur sur un
                                entier qui contient le 
                                nombre d'unites de temps
                                restant pendant lesquelles le 
                                processus process sera execute

    Sorties :
        @return     tproc *    Renvoie la structure du processus
                               ayant le plus petit temp restant
*/

    tnode * process = ready->first;
    tnode * tmp = ready->first;
    tproc * plusCourt = process->proc; 

    for (; tmp != NULL; tmp = tmp->next)
        if((plusCourt->remaining) > tmp->proc->length)
            plusCourt = tmp->proc;

   * delta = QUANTUM;

   return plusCourt;
}
/* --Scheduler srtf-- */

/* --Scheduler edf-- */
tproc * edf(tlist * procs, tlist * ready, int * delta)
{
/*
    Prinicipe de l'ordonnancement dynamique (EDF) :
    -EDF utilise les priorites des taches pour l'ordonnancement. 
    -Il attribue des priorites a la tache en fonction
     du delai absolu.
    -La tache dont le temps restant est le plus proche
     obtient la priorite la plus elevee.
    -Les priorites sont attribuees et modifiees de maniere dynamique.
    -EDF est tres efficace par rapport a d'autres
      algorithmes d'ordonnancement dans les systemes en temps reel.
    -Il peut rendre le taux d'utilisation du processeur
      a environ 100% tout en garantissant les delais de toutes les taches.

    Entrees :
        @param  tlist * procs   c'est un pointeur sur la liste 
                                des autres processus.
        @param  tlist * ready   c'est un pointeur sur la liste
                                des processus prets.
        @param  int * delta     c'est un pointeur sur un entier
                                qui contient le nombre d'unites
                                de temps restant. 
                                pendant lesquelles le processus
                                process sera execute.

    Sorties :
        @return     tproc *    Renvoie la structure du processus
                               le plus prioritaire.
*/

    tproc * processPrioritaire = ready->first->proc;
    tnode * tmp = ready->first;

    for (; tmp != NULL ; tmp = tmp->next)
        if((tmp->proc->activation + tmp->proc->period) <
            (processPrioritaire->activation + processPrioritaire->period))
            processPrioritaire = tmp->proc;
    
    * delta = QUANTUM;

    return processPrioritaire;
}
/* --Scheduler edf-- */

/* --Scheduler rm-- */
tproc * rm(tlist * procs, tlist * ready, int * delta) 
{
/*
    Prinicipe de l'ordonnancement dynamique (RM - Rate Monotonic) :
        -RM est utilise dans les systemes a temps reel
         pour les taches periodiques.
        -C'est a dire que les priorites sont attribuees
          en fonction de la duree de la tache (leur periode).
        -Ce qui veut dire que : plus la periode est courte,
         plus la priorite la tache est elevee.
        -On pouvait aussi utiliser la frequence,
         c'est juste que la frequence est l'inverse
         de la peridode, simple.

    Entrees :
        @param  tlist * procs   c'est un pointeur sur la liste 
                                 des autres processus.
        @param  tlist * ready   c'est un pointeur sur la liste
                                 des processus prets.
        @param  int * delta     c'est un pointeur sur un entier
                                 qui contient le nombre d'unites
                                 de temps restant pendant lesquelles
                                 le processus process sera execute.

    Sorties :
        @return     tproc *    Renvoie la structure du processus
                                le plus prioritaire
*/

    tproc * processPrioritaire = ready->first->proc;
    tnode * tmp = ready->first;

    for (; tmp != NULL; tmp = tmp->next)
        if(processPrioritaire->period > tmp->proc->period)
            processPrioritaire = tmp->proc;

    * delta = QUANTUM;

    return processPrioritaire;
}
/* --Scheduler rm-- */

/* List of ready procs */
tlist ready;

/* List of other procs */
tlist procs;

/* The selected scheduler */
tscheduler scheduler;

/* The scheduling statistics */
tstats stats = {0} ;

/* display usage message */
void usage() {
    fail("usage: sched [fcfs, rr, sjf, srtf, edf, rm]\n");
}

/* simulate a single core scheduler, from time 0 to `max_time` */
void simulate(int max_time) {
    int time=0;
    while(time < max_time) {
        /* Activate process */
        for (tnode * p = procs.first; p != NULL;) {
            tproc * proc = p->proc; 
            p = p->next;

            /* Move every process which should be activated,
             * from the procs list to the ready list */
            if (proc->activation <= time) {
                del(&procs, proc);
                add(&ready, proc);
            }
        }

        /* If any process is ready, then we can schedule! */
        if (ready.first != NULL) {

            int delta = 0;
            /* Call the scheduler */
            tproc * proc = scheduler(&procs, &ready, &delta);

            /* Ensure the scheduler has advanced at least one unit of time */
            assert(delta > 0);
            // comptabiliser le temps total de reponse
            if (proc->remaining == proc->length)
                stats.response += (time - proc->activation);

            /* Output task execution */
            // Si  l'execution d'une tache ne satisfait pas son échéance
            if (proc->period && time < proc->activation + proc->period)
                printf("\\TaskExecution{%d}{%d}{%d}\n", proc->pid, time, time+delta);
            else  // sinon afficher en rouge
                printf("\\TaskExecution[color=red]{%d}{%d}{%d}\n", proc->pid, time, time+delta);

            /* Advance time by delta */
            time += delta;

            /* Remove delta from chosen process */
            proc->remaining-=delta;
 
            /* If the process remaining time is less zero or less, 
             * delete it */ 
            if (proc->remaining <= 0) {
                del(&ready, proc);
                del(&procs, proc);
                // comptabiliser le temps total de completion
                stats.completion += (time - proc->activation);
                // comptabiliser le temps total d’attente
                stats.waiting += (time - proc->activation - proc->length);
                //gerer les taches periodiques
                //Chaque fois qu'une tache finit, si elle est periodique
                // il faut la redemarrer
                if (proc->period) {
                    // mettre a jour ses champs activation et remaining
                    proc->activation = proc->period + proc->activation ;
                    proc->remaining = proc->length;
                    // et la rajouter a la liste des taches
                    add(&procs, proc);
                }
            }
        } 
        /* If no process is ready, just advance the simulation timer */
        else {
            time += 1;
        }

    }
}

int main(int argc, char * argv[]) {

    /* Parse arguments */
    if (argc != 2) usage(); 

    /* Seed random number generator */
    srand(time(NULL) ^ getpid());

    char * method = argv[1]; 

    /* The sched argument should be one of fcfs, rr, sjf, srtf */
    if (strcmp(method, "fcfs") == 0) {
        scheduler = fcfs;
    } 
    else if (strcmp(method, "rr") == 0) {
        scheduler = rr;
    }
    else if (strcmp(method, "sjf") == 0) {
        scheduler = sjf;
    }
    else if (strcmp(method, "srtf") == 0) {
        scheduler = srtf;
    }
    else if (strcmp(method, "edf") == 0) {
        scheduler = edf;
    }
    else if (strcmp(method, "rm") == 0) {
        scheduler = rm;
    }
    else {
        usage();
    }

    /* Compile the task descriptions */
    #include "task_description.h"

    /* Add all tasks to the procs queue */
    for(int i = 0; i < sizeof(tasks)/sizeof(tproc); i ++) {
        add(&procs, &(tasks[i]));
    }
    
    /* Output RTGrid header */
    printf("\\begin{RTGrid}[width=0.8\\textwidth]{%d}{%d}\n", len(&procs), max_time);

    /* Output task arrivals for all tasks */ 
    for (tnode * p = procs.first; p != NULL; p = p->next) {
        printf("\\TaskArrival{%d}{%d}\n", p->proc->pid, p->proc->activation); 
    }

    /* Start scheduling simulation */
    simulate(max_time);

    /* Close RTGrid environment */
    printf("\\end{RTGrid}\\newline\\newline\n");

    /* Print statistics */
    printf("Total completion time = %d\\newline\n", stats.completion);
    printf("Total waiting time = %d\\newline\n", stats.waiting);
    printf("Total response time = %d\\newline\n", stats.response);

    /* Empty the lists if needed */
    del_all(&ready);
    del_all(&procs);

    return 0;
}
