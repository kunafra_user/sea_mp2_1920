MP2 (Partie II - Q12): Métriques 

Q12.Est-ce que les propriétés décrites en cours sont vérifiées ? OUI

Écrivez votre réponse dans le fichier proprietes.txt que vous commiterez dans git.

I) 

INFO: quelques accents manques vu que ces commentaires était dans shed.c

Toutes les propriétés des ordonnanceurs sont vérifiées et les résultats des simulations
obtenues des différents rapports générés (fcfs, rr etc...) sont identiques à ceux obtenus en cours
et comme vous pourrez le voir dans le fichier rapport.ps


2 Principes de l'ordonnancement FCFS :
    - Premier arrive premier servi : FIFO
    - Non Preemptif: il n'interrompt jamais une tache qu'il a commence.

Principes de l'ordonnancement Round Robin (RR) :
    - Chaque processus se voit attribuer un temps fixe (temps quantum) de maniere cyclique, specialement concu
      pour le systeme de temps partage. La liste chaine prete est traitee comme une liste chaine circulaire celle des procs.
      L'ordonanceur CPU parcourt la liste chaine prete, allouant le CPU a chaque processus pour un intervalle
      de temps allant jusqu'a 1 fois le quantum. 
    -Ordonnancement en Tourniquet
    -Pour implementer l'ordonnancement Round Robin, nous conservons la liste chaine prete en tant que liste chaine
      FIFO de processus. De nouveaux processus sont ajoutes a la fin de liste chaine prete. 
      L'ordonnanceur CPU selectionne le premier processus de liste chaine prete, definit une minuterie
      pour interrompre apres un quantum unique et le processus. 

Principes de l'ordonnancement Shortest Job First :
    - SJF est optimal pour reduire le temps d'attente
    - Pour diminuer le temps d'attente, il faut executer d'abord les jobs les plus courts
    - Version non-preemptive
    - Risque de famine

Principes de l'ordonnancement Shortest-Remaining-Time-First :
    - c'est le mode preemptif de l'algorithme SJF dans lequel les taches sont ordonnances
      en fonction du temps restant le plus court.
    - le processus avec le plus petit temps restant jusqu'a la fin est selectionne pour s'executer.
    - Etant donne que le processus en cours d'execution est celui qui dispose par definition du temps le plus court
      et que ce temps ne devrait diminuer qu'au fur et a mesure de l'execution, les processus s'executent toujours jusqu'a
      ce qu'ils se terminent ou qu'un nouveau processus soit ajoute qui necessite un temps plus court.
    - potentiel de famine du processus

II)

Toutes les propriétés des métriques sur la comptabilisation des temps:
-	de complétion
-	de réponse 
-	et total d’attente sont vérifiés à l'issues de simulations et de calculs manuels.

Donc les propriétés décrites en cours sont vérifiées.

